import React from 'react';
import styles from './Styles';
import firebase from 'firebase';

export default class MessageList extends React.Component {
    constructor(){
        super();
        this.state = {messages: []};
    }

    _displayNewMessage(record){
        this._appendChild(record);
        if (record.image !== '') {
            firebase.storage()
                    .ref(record.image)
                    .getDownloadURL()
                    .then(url => {document.getElementById(record.id).src = url});
        }
    }

    _appendChild(record) {
        var innerHTML = <div key={record.id} style={styles.message.rectangle}>
                            {(record.image === '') ? 
                                <span style={styles.message.text}> {record.message} </span> : 
                                <img id={record.id} alt={''} style={styles.message.image} src=''/> }
                            <div style={styles.message.username}> {record.user} </div>
                        </div>;
        var messageList = this.state.messages;
        messageList.push(innerHTML);
        this.setState({messages: messageList})
    }

    componentDidMount() {
        firebase.database().ref('chats').on("child_added", snapshot => {
            this._displayNewMessage(snapshot.val());
        });
    }

    render() {
        return (
            <div id={'messageList'} style={styles.chat.content}> 
                {this.state.messages}
            </div>
        );
    }
};

// firebase.database().ref('chats').on("child_added", snapshot => {
//     var record = snapshot.val();
//     var div = document.createElement('div');
//     var message = (record.image === '') ? 
//         '<div>' + record.message + '</div>' : 
//         '<img id="'+ record.id +'" alt="' + record.image + '" src=""/>';
//     div.innerHTML = message + '<div>' + record.user + '</div>';
//     document.getElementById('messageList').appendChild(div);
//     if (record.image !== '') {
//         firebase.storage().ref(record.image).getDownloadURL().then(url => {
//             document.getElementById(record.id).src = url                    
//         });
//     }
// });