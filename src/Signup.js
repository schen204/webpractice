import React from 'react';
import Axios from 'axios';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import Paper from 'material-ui/Paper';
import Header from './components/Header';
import InputText from './components/InputText';
import Logo from './components/Logo';
import styles from './Styles';

var components = ['Email', 'Password', 'Confirmation', 'Name'];

class Signup extends React.Component {

    constructor(props) {
        super(props);
        this.textInputComponents= [];
        this.handleClick = this.handleClick.bind(this);
        this.onSubmit = this.onSubmit.bind(this);
    }
    
    handleClick() {
        var inputError = false;
        components.forEach(Id => {
            inputError = this.textInputComponents[Id].handleBlur() ? inputError : true;
        });
        if (inputError === false) {
            this.onSubmit();
        }
    }

    onSubmit(){
        Axios.post('https://jsonplaceholder.typicode.com/users', {
            data : {
                email: this.textInputComponents['Email'].state.value,
                password: this.textInputComponents['Password'].state.value,
                name: this.textInputComponents['Name'].state.value,
            }
        })
        .then(function (response) {
            alert('Http status = ' + response.status);
        })
        .catch(function (error) {
            console.log(error);
            alert('Http status = ' + error.status);
            return alert('Post failed.');
        });
        
    }

    componentDidMount() {
        var inputPassword = this.textInputComponents['Password'];
        var inputConfirm = this.textInputComponents['Confirmation'];
        inputPassword.handleBlur = () => {
            if (inputConfirm.state.value) {
                inputConfirm.handleBlur();
            }
            return inputPassword.displayError(inputPassword.state.value);
        }
        inputPassword.handleChange = (e) => {
            if (inputConfirm.state.error) {
                inputConfirm.handleBlur();
            }
            if (inputPassword.state.error !== null) {
                inputPassword.displayError(e.target.value);
            }
            inputPassword.setState({value: e.target.value});
        }
    }

    render() {
        return (
            <div style={styles.root}>
                <Header title={'Signup'} />
                <MuiThemeProvider>
                    <Paper style={styles.paper.board} zDepth={2}>
                        <Logo />
                        <div style={styles.signup.group}>
                            {components.map(Id => 
                                <InputText id={Id} key={Id} ref={el => this.textInputComponents[Id]=el} /> )}
                        </div>
                        <input type='button' 
                            value='Sign up'
                            style={styles.signup.button}
                            onClick={this.handleClick} />
                    </Paper>
                </MuiThemeProvider>   
            </div>
        );
    }
};

export default Signup;
