import React from 'react';
import Main from './Main';
import Signup from './Signup';
import Post from './Posts';
import Chat from './Chat';

// React Router Dom
import {Route} from 'react-router-dom';

const Routes = () => (
    <div>
        <Route exact path="/" component={Main} />
        <Route path='/signup' component={Signup} />
        <Route path='/post' component={Post} />
        <Route path='/chat' component={Chat} />
    </div>
);

/* React Router
import { Router, Route } from 'react-router';

const Routes = (props) => (
    <Router {...props}>
        <Route path='/' component={Signup} />
        <Route path='/signup' component={Signup} />
        <Route path='/post' component={Post} />
    </Router>
);
*/
export default Routes;
