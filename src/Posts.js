import React from 'react';
import Axios from 'axios';
import styles from './Styles';
import Header from './components/Header';
import Post from './Post';

const URL_POST = 'https://jsonplaceholder.typicode.com/posts';
const URL_COMMENT = 'https://jsonplaceholder.typicode.com/comments';

export default class Posts extends React.Component {

    constructor(props) {
        super(props);
        this.state = {posts: []};
    }
    
    _getPostsFromData(postsData, commentsData) {
        var posts = [],
            commentIndex = 0,
            lastCommentId = commentsData[commentsData.length -  1].id;

        postsData.forEach(post => {
            var commentsTemp = [];
            var comment = commentsData[commentIndex];
            while(comment.postId === post.id){
                commentsTemp.push(comment);
                if(comment.id === lastCommentId) {
                    break;
                }
                comment = commentsData[++commentIndex];
            }
            posts.push(<Post key={post.id} id={post.id} data={post} comments={commentsTemp} />)
            this.setState({posts: posts});
        })
    }

    componentWillMount() {
        var postsData = [];
        var commentsData = [];

        Axios.get(URL_POST)
             .then(response => {postsData = response.data})
             .then(
                Axios.get(URL_COMMENT)
                     .then( (response) => {
                        commentsData = response.data;
                        if (postsData.length === 0 || 
                            commentsData.length === 0) 
                            return;
                        this._getPostsFromData(postsData, commentsData);
                     })
                     .catch( error => {console.log(error);})
             )
             .catch( error => {console.log(error);});


        // function getCommentsFromData() {
        //     var commentsTemp = [], 
        //         currentPostIndex = 0,
        //         lastCommentId = commentsData[commentsData.length -  1].id;
            
        //     commentsData.forEach(comment => {
        //         if (postsData[currentPostIndex].id === comment.postId) {
        //             commentsTemp.push(comment);
        //             if(comment.id === lastCommentId) {
        //                 comments.push(commentsTemp);
        //             }
        //         }else {
        //             comments.push(commentsTemp);
        //             commentsTemp = [];
        //             currentPostIndex++;
        //         }
        //     })
        // }

        // function getPostsFromData() {
        //     getCommentsFromData();
        //     postsData.forEach(post => { 
        //         posts.push(
        //             <Post key={post.id} id={post.id} data={post} comments={comments[post.id - 1]} />
        //     )})
            
        // }
    }

    render() {
        return (
            <div style={styles.root}>
                <Header title={'Post'} />
                {this.state.posts}
            </div>
        );
    }
};