import React from 'react'; 
import styles from './Styles';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import Paper from 'material-ui/Paper';
import StarPicker from './components/StarPicker';
import Comment from './components/Comment';
import downArrow from './images/download.svg';
import upArrow from './images/up-arrow.svg';

class Post extends React.Component {

    constructor(props) {
        super(props);
        this.state = {  buttonText: 'Show comment', 
                        arrow: downArrow, 
                        displayComment: {display:'none'}
        };
    }

    handleClick() {
        if(this.state.buttonText === 'Show comment') {
            return this.setState({
                buttonText: 'Hide comment', 
                arrow: upArrow, 
                displayComment: {display: this.props.comments ? '' : 'none'}
            });
        }else{
            return this.setState({
                buttonText: 'Show comment', 
                arrow: downArrow, 
                displayComment: {display:'none'}
            });
        }
    }

    render() {
        return (
            <div id={this.key}>
                <MuiThemeProvider>
                    <Paper style={styles.paper.post} zDepth={1}>
                        <div style={styles.post.top}>
                            <span>{this.props.data.title}</span>
                        </div>
                        <div style={styles.post.main}>
                            <div style={styles.post.content}> {this.props.data.body} </div>
                            <div style={styles.post.starpicker}>
                                <StarPicker id={this.props.id} />
                            </div>
                        </div>
                        <div style={styles.post.bottom} onClick={()=>this.handleClick()}>
                            <div style={styles.post.bottomText}> {this.state.buttonText} </div>
                            <img src={this.state.arrow} alt='' style={styles.arrow} />
                        </div>
                    </Paper>
                </MuiThemeProvider>
                <div style={this.state.displayComment}>
                    <MuiThemeProvider>
                        <Paper style={styles.comment.board} zDepth={1}>
                            <div style={styles.comment.blank} />
                            {this.props.comments.map(element => <Comment key={element.id} data={element} />)}
                        </Paper>
                    </MuiThemeProvider>
                </div>
            </div>
        );
    }
};

export default Post;