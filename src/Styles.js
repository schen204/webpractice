var styles = {
    root:{
        display:        'flex',
        flexDirection:  'column',
        alignItems:     'center',
    },

    header: {
        display:        'flex',
        flex:           '1 0 auto',
        justifyContent: 'center',
        alignSelf:      'stretch',
        marginLeft:     -10,
        marginTop:      -8,
        marginRight:    -10,
    },

    content: {
        display:        'flex',
        flexDirection:  'column',
        justifyContent: 'space-around',
        alignItems:     'center',
    },

    title: {
        display:        'flex',
        flex:           '0 0 auto',
        alignItems:     'center',
        fontFamily:     'Avenir',
        fontWeight:     'bold',
        height:         88,
        fontSize:       36,
    },

    none: {
        display:        'none',
    },
    paper: {
        header: {
            display:        'flex',
            justifyContent: 'center',
            flex:           '1 1 auto',
            height:         88,
            width:          1440,
        },

        board: {
            display:        'flex',
            flexDirection:  'column',
            alignItems:     'center',
            width:          466,
            height:         623,
            borderRadius:   16,
            marginTop:      88,
            marginBottom:   225,
        },

        oval: {
            display:        'flex',
            alignItems:     'center',
            overflow:       'hidden',
            width:          108,
            height:         108,
            marginTop:      49,
        },

        post: {
            display:        'flex',
            flexDirection:  'column',
            alignItems:     'center',
            width:          1113,
            marginTop:      36,
        },

        comment: {
            display:        'flex',
            flexDirection:  'column',
            width:          1040,
            marginBottom:   36,
        }
    },

    post: {
        top: {
            display:        'flex',
            flexDirection:  'column',
            alignSelf:      'stretch',
            justifyContent: 'center',
            alignItems:     'center',
            height:         45,
            background:     '#C8C8CC',
        },

        main: {
            display:        'flex',
            flexDirection:  'column',
            alignSelf:      'stretch',
            justifyContent: 'center',
            paddingLeft:    37,
            paddingRight:   37,
        },

        content: {
            display:        'flex',
            fontSize:       16,
            fontFamily:     'Avenir',
            color:          '#1D1D26',
            alignSelf:      'stretch',
            marginTop:      24,
        },

        starpicker: {
            alignSelf:      'flex-end',
            marginTop:      19,
        },

        bottom: { 
            display:        'flex',
            flexDirection:  'row',
            alignSelf:      'stretch',
            alignItems:     'center',
            height:         45,
            background:     '#D8D8D8',
            outline:        'none',
            userSelect:     'none',
        },

        bottomText: {
            fontFamily:     'Avenir',
            fontSize:       16,
            paddingLeft:    36,
            width:          1005,
        },

    },

    comment: {
        board: {
            display:        'flex',
            flexDirection:  'column',
            alignItems:     'center',
        },

        main: {
            alignSelf:      'stretch',
        },

        content: {
            paddingLeft:    36,
            paddingRight:   36,
            marginTop:      36,
        },

        bottom: {
            display:        'flex',
            flexDirection:  'row',
            background:     'rgba(155, 155, 155, 0.54)',
            marginTop:      65,
        },

        
        left: {
            display:        'flex',
            alignItems:     'center',
            height:         45,
            width:          698,
            paddingLeft:    29,
            borderRight:    'solid #979797',
        },

        right: {
            display:        'flex',
            alignItems:     'center',
            height:         45,
            width:          339,
            paddingLeft:    36,
        },

        blank: {
            height:         36,
            alignSelf:      'stretch',
        },
    },

    chat: {
        main: {
            display:        'flex',
            flexDirection:  'column',
            width:          975,
            marginTop:      36,
            marginBottom:   69,
        },

        content: {
            minHeight:      900,
            alignSelf:      'stretch',
        },

        bottom: {
            display:        'flex',
            flexDirection:  'row',
            alignSelf:      'stretch',
            alignItems:     'center',
            marginTop:      20,
            marginBottom:   75,
            paddingLeft:    53,
        },

        rectangle: {
            borderBottom:   'thin #000 solid',
            height:         50,
            width:          303,
            
        },

        newMessage: {
            width:          303,
            height:         16,
            fontSize:       16,
            // lineHeight:     16,
            borderWidth:    0,
            marginTop:      27,
            outline:        'none',
        },
        
        send: {
            paddingLeft:    46,
        },

        button: {
            background:     '#CE3E3E',
            color:          '#FFFFFF',
            width:          200,
            height:         50,
            borderRadius:   16,
            borderWidth:    0,
            fontSize:       16,
        },

        selectImage: {
            width:          50,
            height:         50,
            paddingLeft:    46,
        },
        
    },

    message: {
        rectangle: {
            fontFamily:     'PingFangSC',
            width:          663,
            marginTop:      45,
            paddingLeft:    53,
        },

        text: {
            display:        'flex',
            alignSelf:      'stretch',
            fontSize:       16,
            wordWrap:       'break-word',
            wordBreak:      'break-all',
            color:          'rgba(0, 0, 0, 0.87)',
        },

        image: {
            maxWidth:       400,
            maxHeight:      300,
        },

        username: {
            fontSize:       14,
            color:          'rgba(0, 0, 0, 0.54)',
            marginTop:      11,
        },
    },

    logo: {
        width:          'auto',
        height:         83,
    },
    
    arrow: {
        width:          30,
        height:         30,
    },

    star: {
        height:         30,
        width:          29,
    },

    signup: {
        group: {
            fontFamily:     'Avenir',
            color:          '#000000',
            marginTop:      35,
        },

        rectangle: {
            width:          305,
            height:         60,
        },

        text: {
            marginLeft:     2,
            marginTop:      16,
            width:          303,
            height:         16,
            fontSize:       16,
            // lineHeight:     16,
            borderWidth:    0,
            outline:        'none',
        },

        line: {
            borderBottom:   'thin #000 solid',
            height:         8,
            width:          303,
        },

        line_error: {
            borderBottom:   'thin #f00 solid',
            height:         8,
        },
       
        error: {
            marginTop:      8,
            marginLeft:     2,
            fontSize:       12,
            color:          '#FF0000',
            height:         12,
        },

        button: {
            background:     '#CE3E3E',
            color:          '#FFFFFF',
            width:          200,
            height:         50,
            borderRadius:   16,
            borderWidth:    0,
            fontSize:       16,
            marginTop:      91,
        },
    },
    
};

export default styles;