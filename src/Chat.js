import React from 'react';
import styles from './Styles';
import Header from './components/Header';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import Paper from 'material-ui/Paper';
import firebase from 'firebase';
import Picture from './images/picture.svg';
import MessageList from './MessageList';

var user = '李寻欢', userId = 1;

//var user = '阿飞', userId = 2;

//var user = '孙小红', userId = 3;

export default class Chat extends React.Component {
    
    componentWillMount(){
        document.body.style.background = '#D8D8D8';
        this._initFirebase();
    }

    _initFirebase() {
        var config = {
            apiKey: 'AIzaSyAfrBIQwVqVNeDY8MItsOSlsM222xFKWGc',
            authDomain: 'chatroom-bdb39.firebaseapp.com',
            databaseURL: 'https://chatroom-bdb39.firebaseio.com',
            projectId: 'chatroom-bdb39',
            storageBucket: 'chatroom-bdb39.appspot.com',
            messagingSenderId: '62862052331'
        };
        firebase.initializeApp(config);
    }

    _sendMessage(message, image) {
        var date = new Date();
        var time = date.getHours() + ':' + date.getMinutes() + ':' + date.getSeconds();
        firebase.database()
                .ref('chats/' + date.getTime() + '_' + userId)
                .set({
                    id: date.getTime(),
                    message: message,
                    user: user,
                    time: time,
                    image: image
        });
    }

    getInput() {
        var message = this.message.value;
        if (message === '') return;
        this.message.value = '';
        this._sendMessage(message, '');
    }

    sendImage(e) {
        var date = new Date();
        var image = this.fileOpen.files[0];
        var imagePath = 'images/' + date.getTime() + '_' + userId +image.name;
        firebase.storage()
                .ref(imagePath)
                .put(image)
                .on('state_changed', 
                    null, 
                    null, 
                    () => {this._sendMessage('', imagePath)} //on success
        );
    }

    render() {
        return (
            <div style={styles.root}>
                <Header title={'Chat'} />
                <MuiThemeProvider>
                    <Paper style={styles.chat.main} zDepth={2}>
                        <MessageList />
                        <div style={styles.chat.bottom}>
                            <div style={styles.chat.rectangle}>
                                <input type='text' style={styles.chat.newMessage} 
                                    ref={el => {this.message = el}}
                                    placeholder='请输入聊天内容...' />
                                <div style={styles.chat.line} />
                            </div>
                            <div style={styles.chat.send}>
                                <input type='button' 
                                    value='Send'
                                    style={styles.chat.button}
                                    onClick={() => this.getInput()} />
                            </div>
                            <input type='file' 
                                ref={el=> {this.fileOpen = el}}
                                style={styles.none}
                                onChange={() => this.sendImage()}
                                accept='image/*' />
                            <img alt='' src={Picture} 
                                style={styles.chat.selectImage} 
                                onClick={() => this.fileOpen.click()} />
                        </div>
                    </Paper>
                </MuiThemeProvider>
            </div>
        );
    }
};