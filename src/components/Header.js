import React from 'react';
import styles from '../Styles.js';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import Paper from 'material-ui/Paper';

class Header extends React.Component {
    render() {
        return ( 
            <div style={styles.header}>
                <MuiThemeProvider>
                    <Paper style={styles.paper.header} zDepth={2}>
                        <div style={styles.title}> {this.props.title} </div> 
                    </Paper>
                </MuiThemeProvider> 
            </div>
        );
    }
};

export default Header;