import React from 'react';
import Paper from 'material-ui/Paper';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import styles from '../Styles';

export default class Comment extends React.Component {
    render() {
        return (
            <MuiThemeProvider>
                <Paper style={styles.paper.comment} zDepth={1}>
                    <div style={styles.comment.main}>
                        <div style={styles.comment.content}>{this.props.data.body}</div>
                    </div>
                    <div style={styles.comment.bottom}>
                        <div style={styles.comment.left}>
                            <span>{this.props.data.name}</span>
                        </div>
                        <div style={styles.comment.right}>
                            <span>{this.props.data.email}</span>
                        </div>
                    </div>
                </Paper>
            </MuiThemeProvider>
        );
    }
};