import React from 'react';
import styles from '../Styles';

const ErrorInfo = {
    Empty_Email:            'Please enter your email address.',
    Empty_Password:         'Please enter your password.',
    Empty_Confirmation:     'Please repeat your password to confirm.',
    Empty_Name:             'Please enter your name.',
    Invalid_Email:          'Invalid email address.', 
    Invalid_Password:       'Password must has 6 to 15 characters.',
    Invalid_Confirmation:   'Confirmation is different from the password',
    Invalid_Name:           'Name must has at most 20 characters'
};

const PlaceHolders = {
    Email :                 'Email', 
    Password:               'Password', 
    Confirmation:           'Password Confirmation', 
    Name:                   'Name'
};


class InputText extends React.Component {

    constructor(props) {
        super(props);
        this.state = {value: '', error: null};
        this.handleChange = this.handleChange.bind(this);
        this.handleBlur   = this.handleBlur.bind(this);
        this.displayError = this.displayError.bind(this);
        this._isValid = this._isValid.bind(this);
    }

    handleChange(e) {
        this.setState({value: e.target.value});
        return this.state.error !== null ? this.displayError(e.target.value) : true;
    }

    handleBlur(){
        return this.displayError(this.state.value);
    }

    displayError(val){
        var err = !val ? 'Empty_' : (!this._isValid(val) ?  'Invalid_' : null);
        this.setState({error: (err) ? ErrorInfo[err + this.props.id] : null});
        return err === null;
    }

    _isValid(val){
        switch(this.props.id){
        case 'Email':
            return /^([a-zA-Z0-9_.-])+@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/.test(val);
        case 'Password':
            return val.length >= 6 &&  val.length <= 15;
        case 'Confirmation':
            return document.getElementById('Password').value === document.getElementById('Confirmation').value;
        case 'Name':
            return val.length <= 20;
        default:
            return;
        }
    }
    render() {
        var id = this.props.id;
        return (
            <div style={styles.signup.rectangle}>
                <input type='text'
                    style={styles.signup.text}
                    id={this.props.id}
                    ref={this.props.inputRef}
                    placeholder={PlaceHolders[id]}
                    value={this.state.value}
                    onChange={this.handleChange}
                    onBlur={this.handleBlur} />
                <div style={ this.state.error ? styles.signup.line_error : styles.signup.line} />
                <div style={styles.signup.error}>
                    {this.state.error}
                </div>
            </div>
        );
    }
};

export default InputText;