import React from 'react';
import styles from '../Styles';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import Paper from 'material-ui/Paper';
import logo from '../images/logo.JPG';

class Logo extends React.Component {
    render() {
        return (
            <MuiThemeProvider>
                <Paper  style={styles.paper.oval} zDepth={2} circle={true}>
                    <img src={logo} alt='' style={styles.logo}/>
                </Paper>
            </MuiThemeProvider>
        );
    }
};

export default Logo;