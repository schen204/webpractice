import React from 'react';
import styles from '../Styles';
import star from '../images/star.svg';
import starEmpty from '../images/star_empty.svg';

export default class StarPicker extends React.Component {

    constructor() {
        super();
        this.state = {level: 0, star: Array(5).fill(starEmpty)};
    }

    componentDidMount() {
        var value = localStorage.getItem(this.props.id);
        this.setState({level: value}, this.handleChange(value));
    }

    handleClick(e){
        this.setState({level: e.target.id}, localStorage.setItem(this.props.id, e.target.id), this.handleChange(e.target.id));
    }

    handleChange(lvl) {
        this.setState({star: Array(5).fill(starEmpty).fill(star, 0, lvl)});
    }

    render() {
        return (
            <div>
                {[1, 2, 3, 4, 5].map(id =>
                    <img id={id} key={id} alt='' 
                        style={styles.star} 
                        src={this.state.star[id - 1]} 
                        onClick={e => this.handleClick(e)} 
                        onMouseOver={e => this.handleChange(e.target.id)}
                        onMouseOut={() => this.handleChange(this.state.level)} /> )}
            </div>
        );
    }
};
