import React from 'react';
import {Redirect} from 'react-router-dom';

export default class Main extends React.Component {
  render() {
    return (
        <Redirect from="/" to="/signup" />
    );
  }
}